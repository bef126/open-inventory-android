package com.appspot.openinventory;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class SplashActivity extends ActionBarActivity  {

    private OpenInventoryApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        app = (OpenInventoryApplication) this.getApplicationContext();

        TextView tv = (TextView) findViewById(R.id.version);
        tv.setText(app.getVersion());

        // show splash page for at least 2 seconds
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                nextStep();
            }
        }, (1000 * 2));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        app.handleMenuItem(item, this);

        return super.onOptionsItemSelected(item);
    }

    private void nextStep() {
        finish();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
