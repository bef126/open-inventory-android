package com.appspot.openinventory;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.appspot.open_inventory.openinventory.model.OpenInventoryUpdateInventoryResponse;


public class AdjustInventoryActivity extends ActionBarActivity {

    private String key;
    private String name;
    private long currentInventory;
    private NumberPicker np;
    private OpenInventoryApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adjust_inventory);
        app = (OpenInventoryApplication) this.getApplicationContext();
        key = getIntent().getStringExtra("key");
        name = getIntent().getStringExtra("name");
        currentInventory = getIntent().getLongExtra("inventory", 0);
        np = (NumberPicker) findViewById(R.id.numberPicker2);
        np.setMaxValue(50);
        np.setMinValue(1);
        np.setWrapSelectorWheel(true);

        TextView tv = (TextView) findViewById(R.id.name);
        tv.setText(name);
        tv = (TextView) findViewById(R.id.currentInventory);
        tv.setText(""+currentInventory);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        app.handleMenuItem(item, this);

        return super.onOptionsItemSelected(item);
    }

    public void increment(View view) {
        final long inventory = np.getValue();
        final Context me = this;

        class SendPostReqAsyncTask extends AsyncTask<String, Void, OpenInventoryUpdateInventoryResponse> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", "Please Wait", false);
                super.onPreExecute();
            }

            @Override
            protected OpenInventoryUpdateInventoryResponse doInBackground(String... p) {
                try {
                    return Api.incrementInventory(app.getCredential(), key, inventory);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(OpenInventoryUpdateInventoryResponse result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if(result == null){
                    showError();
                } else {
                    finish();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    public void decrement(View view) {
        NumberPicker np = (NumberPicker) findViewById(R.id.numberPicker2);
        final long inventory = np.getValue();
        final Context me = this;

        class SendPostReqAsyncTask extends AsyncTask<String, Void, OpenInventoryUpdateInventoryResponse> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", "Please Wait", false);
                super.onPreExecute();
            }

            @Override
            protected OpenInventoryUpdateInventoryResponse doInBackground(String... p) {
                try {
                    return Api.decrementInventory(app.getCredential(), key, inventory);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(OpenInventoryUpdateInventoryResponse result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if(result == null){
                    showError();
                } else {
                    finish();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void showError(){
        app.showDialog("Error", "Please try again.", this);
    }
}
