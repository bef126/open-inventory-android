package com.appspot.openinventory;

import com.appspot.open_inventory.openinventory.Openinventory;
import com.appspot.open_inventory.openinventory.model.OpenInventoryMyInventoryResponse;
import com.appspot.open_inventory.openinventory.model.OpenInventoryProductExistsResponse;
import com.appspot.open_inventory.openinventory.model.OpenInventorySuggestionsResponse;
import com.appspot.open_inventory.openinventory.model.OpenInventoryUpdateInventoryResponse;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

public class Api {

    //endpointscfg.py get_client_lib java -bs gradle -o . api.OpenInventoryApi

    public static OpenInventoryProductExistsResponse productExists(GoogleAccountCredential credential, String upc){
        try {
            Openinventory apiServiceHandle = Constants.getApiServiceHandle(credential);
            Openinventory.ProductExists m = apiServiceHandle.productExists(upc);
            OpenInventoryProductExistsResponse r = m.execute();
            return r;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static boolean createProduct(GoogleAccountCredential credential, String name, String upc, long inventory){
        try {
            Openinventory apiServiceHandle = Constants.getApiServiceHandle(credential);
            Openinventory.CreateProduct m = apiServiceHandle.createProduct(upc, name, inventory);
            m.execute();
            return true;
        } catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static OpenInventoryUpdateInventoryResponse incrementInventory(GoogleAccountCredential credential, String key, long delta){
        try {
            Openinventory apiServiceHandle = Constants.getApiServiceHandle(credential);
            Openinventory.IncrementInventory m = apiServiceHandle.incrementInventory(key, delta);
            OpenInventoryUpdateInventoryResponse r = m.execute();
            return r;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static OpenInventoryUpdateInventoryResponse decrementInventory(GoogleAccountCredential credential, String key, long delta){
        try {
            Openinventory apiServiceHandle = Constants.getApiServiceHandle(credential);
            Openinventory.DecrementInventory m = apiServiceHandle.decrementInventory(key, delta);
            OpenInventoryUpdateInventoryResponse r = m.execute();
            return r;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static OpenInventorySuggestionsResponse nameSuggestions(GoogleAccountCredential credential, String upc){
        try {
            Openinventory apiServiceHandle = Constants.getApiServiceHandle(credential);
            Openinventory.SuggestName m = apiServiceHandle.suggestName(upc);
            OpenInventorySuggestionsResponse r = m.execute();
            return r;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static OpenInventoryMyInventoryResponse myInventory(GoogleAccountCredential credential){
        try {
            Openinventory apiServiceHandle = Constants.getApiServiceHandle(credential);
            Openinventory.MyInventory m = apiServiceHandle.myInventory();
            OpenInventoryMyInventoryResponse r = m.execute();
            return r;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
