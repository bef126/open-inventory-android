package com.appspot.openinventory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

public class OpenInventoryApplication extends Application {
    private GoogleAccountCredential credential;
    private SharedPreferences sp;

    @Override
    public void onCreate() {
        super.onCreate();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    public void setCredential(GoogleAccountCredential c){
        this.credential = c;
    }

    public GoogleAccountCredential getCredential(){
        return this.credential;
    }

    public void savePref(String key, String value){
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public String stringPref(String key){
        return sp.getString(key, null);
    }

    public void removePref(String key){
        SharedPreferences.Editor edit = sp.edit();
        edit.remove(key);
        edit.commit();
    }

    public boolean hasPref(String key){
        return sp.contains(key);
    }

    public String getVersion(){
        String version = "0.0";
        try {
            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }

    public void aboutDialog(Activity me){
        showDialog(getString(R.string.app_name), "Version: " + getVersion(), me);
    }

    public void showDialog(String title, String msg, Activity me){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(me);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(msg)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void removeLogin(){
        removePref(Constants.EMAIL_LABEL);
    }

    public void storeLogin(String value){
        savePref(Constants.EMAIL_LABEL, value);
    }

    public void handleMenuItem(MenuItem item, Activity me){
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            removeLogin();
            System.exit(1);
        } else if(id == R.id.action_about){
            aboutDialog(me);
        }
    }
}
