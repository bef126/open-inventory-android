package com.appspot.openinventory;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.appspot.open_inventory.openinventory.model.OpenInventoryProductExistsResponse;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;


public class MainActivity extends ActionBarActivity {

    public static final int SCAN_ACTIVITY = 1;
    public static final int REQUEST_ACCOUNT = 2;
    private static final String TAG = "MainActivity";
    private OpenInventoryApplication app;
    private GoogleAccountCredential c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app = (OpenInventoryApplication) this.getApplicationContext();
        c = GoogleAccountCredential.usingAudience(this, Secrets.AUDIENCE);
        if(app.hasPref(Constants.EMAIL_LABEL)){
            getAccessToken(app.stringPref(Constants.EMAIL_LABEL));
        } else {
            startActivityForResult(c.newChooseAccountIntent(), REQUEST_ACCOUNT);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        app.handleMenuItem(item, this);

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SCAN_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("result");
                Log.v(TAG, result);
                doesProductExist(result);
            } else if(resultCode == 999) {
                app.showDialog("Error", "Scan timed out. Please try again.", this);
            }
        } else if (requestCode == REQUEST_ACCOUNT) {
            if (data != null && data.getExtras() != null) {
                String accountName = data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);
                if (accountName != null) {
                    getAccessToken(accountName);
                } else {
                    startActivityForResult(c.newChooseAccountIntent(), REQUEST_ACCOUNT);
                }
            } else {
                startActivityForResult(c.newChooseAccountIntent(), REQUEST_ACCOUNT);
            }
        }
    }

    public void scan(View view) {
        Intent i = new Intent(this, ScanActivity.class);
        startActivityForResult(i, SCAN_ACTIVITY);
    }

    private void doesProductExist(final String upc){
        final Context me = this;
        class SendPostReqAsyncTask extends AsyncTask<String, Void, OpenInventoryProductExistsResponse> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", "Looking up UPC", false);
                super.onPreExecute();
            }

            @Override
            protected OpenInventoryProductExistsResponse doInBackground(String... p) {
                try {
                    return Api.productExists(app.getCredential(), upc);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(OpenInventoryProductExistsResponse result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if(result == null){
                    createProduct(upc);
                } else {
                    adjustInventory(result.getPkey(), result.getName(), result.getInventory());
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void createProduct(String upc){
        Intent i = new Intent(this, CreateProductActivity.class);
        i.putExtra("upc", upc);
        startActivity(i);
    }

    private void adjustInventory(String key, String name, long inventory){
        Intent i = new Intent(this, AdjustInventoryActivity.class);
        i.putExtra("key", key);
        i.putExtra("name", name);
        i.putExtra("inventory", inventory);
        startActivity(i);
    }

    private void getAccessToken(final String accountName){
        final Context me = this;
        class SendPostReqAsyncTask extends AsyncTask<String, Void, Boolean> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", "Retrieving Access Token", false);
                super.onPreExecute();
            }

            @Override
            protected Boolean doInBackground(String... p) {
                try {
                    c.setSelectedAccountName(accountName);
                    String accessToken = c.getToken();
                    Log.d(TAG, accessToken);
                    app.setCredential(c);
                    app.storeLogin(accountName);
                    return true;
                } catch(Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if(result == false){
                    noAccessToken();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void noAccessToken(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Error");
        alertDialogBuilder
                .setMessage("Error retrieving access token.")
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void myInventory(View view) {
        Intent i = new Intent(this, InventoryActivity.class);
        startActivity(i);
    }
}
