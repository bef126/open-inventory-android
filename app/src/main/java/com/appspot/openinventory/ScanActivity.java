package com.appspot.openinventory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

//https://github.com/dm77/barcodescanner

public class ScanActivity extends Activity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    private Handler handler;
    private Runnable run;
    private OpenInventoryApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        app = (OpenInventoryApplication) this.getApplicationContext();

        // timeout after 30 seconds
        handler = new Handler();
        run = new Runnable() {
            public void run() {
                timeout();
            }
        };
        handler.postDelayed(run, (1000 * 30));
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            handler.removeCallbacks(run);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        Intent returnIntent = getIntent();
        returnIntent.putExtra("result", rawResult.getText());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        app.handleMenuItem(item, this);

        return super.onOptionsItemSelected(item);
    }

    private void timeout(){
        Intent returnIntent = getIntent();
        returnIntent.putExtra("result", "");
        setResult(999, returnIntent);
        finish();
    }
}
