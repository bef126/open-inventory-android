package com.appspot.openinventory;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.appspot.open_inventory.openinventory.model.OpenInventorySuggestionsResponse;

import java.util.ArrayList;
import java.util.List;


public class CreateProductActivity extends ActionBarActivity {

    private String upc;
    private NumberPicker np;
    private OpenInventoryApplication app;
    private List<String> suggestions;
    private EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);
        app = (OpenInventoryApplication) this.getApplicationContext();
        upc = getIntent().getStringExtra("upc");
        np = (NumberPicker) findViewById(R.id.numberPicker);
        np.setMaxValue(50);
        np.setMinValue(1);
        np.setWrapSelectorWheel(true);
        etName = (EditText) findViewById(R.id.editName);
        getNameSuggestions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        app.handleMenuItem(item, this);

        return super.onOptionsItemSelected(item);
    }

    public void createProduct(View view) {
        EditText et = (EditText) findViewById(R.id.editName);
        final String name = et.getText().toString();
        final long inventory = np.getValue();
        final Context me = this;

        class SendPostReqAsyncTask extends AsyncTask<String, Void, Boolean> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", "Please Wait", false);
                super.onPreExecute();
            }

            @Override
            protected Boolean doInBackground(String... p) {
                try {
                    return Api.createProduct(app.getCredential(), name, upc, inventory);
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if (result) {
                    finish();
                } else {
                    showError();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void showError() {
        app.showDialog("Error", "Error creating product. Please try again.", this);
    }

    private void getNameSuggestions() {
        final Context me = this;

        class SendPostReqAsyncTask extends AsyncTask<String, Void, OpenInventorySuggestionsResponse> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", "Please Wait", false);
                super.onPreExecute();
            }

            @Override
            protected OpenInventorySuggestionsResponse doInBackground(String... p) {
                try {
                    return Api.nameSuggestions(app.getCredential(), upc);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(OpenInventorySuggestionsResponse result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if (result != null) {
                    suggestions = result.getSuggestions();
                    if(suggestions == null){
                        suggestions = new ArrayList<String>();
                    }
                } else {
                    suggestions = new ArrayList<String>();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    public void viewSuggestions(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Name Suggestions")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        if (suggestions.size() > 0) {
            alertDialogBuilder.setItems(suggestions.toArray(new String[0]), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    etName.setText(suggestions.get(which));
                }
            });
        } else {
            alertDialogBuilder.setMessage("No Suggestions");
        }
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}