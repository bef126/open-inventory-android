package com.appspot.openinventory;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.appspot.open_inventory.openinventory.model.OpenInventoryMyInventoryResponse;
import com.appspot.open_inventory.openinventory.model.OpenInventoryProductExistsResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class InventoryActivity extends ActionBarActivity {

    private OpenInventoryApplication app;
    private ListView lv;
    private List<Map<String , String>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
        app = (OpenInventoryApplication) this.getApplicationContext();
        lv = (ListView) findViewById(R.id.listView);
        getInventory();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        app.handleMenuItem(item, this);

        return super.onOptionsItemSelected(item);
    }

    private void getInventory() {
        final Context me = this;

        class SendPostReqAsyncTask extends AsyncTask<String, Void, OpenInventoryMyInventoryResponse> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", "Retrieving Inventory", false);
                super.onPreExecute();
            }

            @Override
            protected OpenInventoryMyInventoryResponse doInBackground(String... p) {
                try {
                    OpenInventoryMyInventoryResponse r = Api.myInventory(app.getCredential());
                    data = new ArrayList<Map<String,String>>();
                    List<OpenInventoryProductExistsResponse> products = r.getProducts();
                    for (OpenInventoryProductExistsResponse item : products) {
                        Map<String,String> map = new HashMap<String, String>();
                        map.put("name", item.getName());
                        map.put("count", ""+item.getInventory());
                        map.put("key", item.getPkey());
                        data.add(map);
                    }
                    return r;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(OpenInventoryMyInventoryResponse result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if (result != null) {
                    refreshList();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void refreshList(){
        if(data == null){
            data = new ArrayList<Map<String,String>>();
        }
        ArrayAdapter itemsAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_2, android.R.id.text1, data)
        {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                Map<String,String> map = data.get(position);
                text1.setText(map.get("name"));
                text2.setText(map.get("count"));
                return view;
            }
        };
        lv.setAdapter(itemsAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemClicked(position);
            }
        });
    }

    private void itemClicked(int position){
        finish();
        Intent i = new Intent(this, AdjustInventoryActivity.class);
        Map<String,String> map = data.get(position);
        i.putExtra("key", map.get("key"));
        i.putExtra("name", map.get("name"));
        i.putExtra("inventory", Long.parseLong(map.get("count")));
        startActivity(i);
    }
}
