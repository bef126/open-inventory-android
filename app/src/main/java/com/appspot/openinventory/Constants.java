package com.appspot.openinventory;

import com.appspot.open_inventory.openinventory.Openinventory;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

public class Constants {
    public static final JsonFactory JSON_FACTORY = new AndroidJsonFactory();
    public static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();

    public static final String EMAIL_LABEL = "email";

    public static Openinventory getApiServiceHandle(GoogleAccountCredential credential) {
        Openinventory.Builder builder = new Openinventory.Builder(Constants.HTTP_TRANSPORT, Constants.JSON_FACTORY, credential);

        // for testing on local dev server
        //builder.setRootUrl("http://192.168.1.64:8080/_ah/api/");
        //builder.setApplicationName("open-inventory");

        return builder.build();
    }
}
